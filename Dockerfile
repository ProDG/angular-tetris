FROM hub.t3c.pp.ua/node:16 AS angular_build

WORKDIR /app
COPY . /app

RUN npm install
RUN npm run build


FROM hub.t3c.pp.ua/nginx:1.22.0-alpine
COPY --from=angular_build /app/dist/angular-tetris /usr/share/nginx/html
